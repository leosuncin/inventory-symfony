<?php

declare(strict_types=1);

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

final class ProductAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('id')
            ->add('name')
            ->add('model')
            ->add('serial')
            ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('id')
            ->add('name')
            ->add('model')
            ->add('serial')
            ->add('brand.name')
            ->add('category.name')
            ->add('unit.name')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->add('name')
            ->add('model')
            ->add('serial')
            ->add('brand', EntityType::class, [
                'class' => 'App\Entity\Brand',
                'choice_label' => 'name',
                'placeholder' => 'Choose an option',
            ])
            ->add('category', EntityType::class, [
                'class' => 'App\Entity\Category',
                'choice_label' => 'name',
                'placeholder' => 'Choose an option',
            ])
            ->add('unit', EntityType::class, [
                'class' => 'App\Entity\Unit',
                'choice_label' => 'name',
                'placeholder' => 'Choose an option',
            ])
            ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->add('id')
            ->add('name')
            ->add('model')
            ->add('serial')
            ->add('brand.name')
            ->add('category.name')
            ->add('unit.name')
            ;
    }
}
