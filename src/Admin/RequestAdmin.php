<?php

declare(strict_types=1);

namespace App\Admin;

use App\Form\RequestDetailType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
//use Sonata\Form\Type\CollectionType;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

final class RequestAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('id')
            ->add('code')
            ->add('requestedAt');
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('id')
            ->add('code')
            ->add('requestedAt')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->add('code')
            ->add('provider', ModelAutocompleteType::class, [
                'class' => 'App\Entity\Provider',
                'property' => 'name',
                'placeholder' => 'Choose an option',
                'to_string_callback' => function ($entity) {
                    return $entity->getName();
                },
            ])
            ->add('details', CollectionType::class, [
                'entry_type' => RequestDetailType::class,
                'by_reference' => false,
                'allow_add' => true,
                'delete_empty' => true,
                'prototype' => true,
            ])
//            ->add('details', CollectionType::class, [
//                'by_reference' => false,
//                'type' => RequestDetailType::class,
//            ], [
//                'edit' => 'inline',
//                'inline' => 'table',
//                'sortable' => 'position'
//            ])
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->add('id')
            ->add('code')
            ->add('requestedAt')
            ->add('details', null, [
                'template' => 'request/show_field.html.twig',
            ]);
    }
}
