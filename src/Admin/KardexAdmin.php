<?php

declare(strict_types=1);

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;

final class KardexAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('id')
            ->add('product.name')
            ->add('unitPrice')
            ->add('entry')
            ->add('egress')
            ->add('createdAt')
            ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('id')
            ->add('product.name')
            ->add('entry')
            ->add('egress')
            ->add('unitPrice')
            ->add('createdAt')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->add('product', ModelAutocompleteType::class, [
                'property' => 'name',
                'to_string_callback' => function ($entity) {
                    return $entity->getName();
                },
            ])
            ->add('entry', null, [
                'attr' => [
                    'min' => '0',
                    'step' => '1',
                ],
            ])
            ->add('egress', null, [
                'attr' => [
                    'min' => '0',
                    'step' => '1',
                ],
            ])
            ->add('unitPrice', MoneyType::class, [
                'currency' => 'USD'
            ])
            ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->add('id')
            ->add('product.name')
            ->add('entry')
            ->add('egress')
            ->add('unitPrice')
            ->add('createdAt')
            ;
    }
}
