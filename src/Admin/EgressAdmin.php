<?php

declare(strict_types=1);

namespace App\Admin;

use App\Form\EgressDetailType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;

final class EgressAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('id')
            ->add('egressedAt')
            ->add('total')
            ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('id')
            ->add('egressedAt')
            ->add('total')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->add('details', CollectionType::class, [
                'entry_type' => EgressDetailType::class,
                'by_reference' => false,
                'allow_add' => true,
                'delete_empty' => true,
                'prototype' => true,
            ])
            ->add('total', MoneyType::class, [
                'currency' => 'USD',
                'attr' => [
                    'min' => 0,
                    'step' => '0.01',
                ],
            ])
            ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->add('id')
            ->add('egressedAt')
            ->add('details', null, [
                'template' => 'egress/show_field.html.twig',
            ])
            ->add('total')
            ;
    }
}
