<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190403032303 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Create table for kardex';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE kardex_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE kardex (
            id INT NOT NULL,
            product_fk INT NOT NULL,
            unit_price NUMERIC(10, 2) NOT NULL DEFAULT 0.00,
            entry INT DEFAULT NULL,
            egress INT DEFAULT NULL,
            created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT  NOW(),
            CONSTRAINT kardex_PK PRIMARY KEY(id),
            CONSTRAINT karder_product_FK FOREIGN KEY (product_fk) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE
        )');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE kardex_id_seq CASCADE');
        $this->addSql('DROP TABLE kardex');
    }
}
