<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190402173011 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Create Unit\'s table';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE "unit_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE "unit" (
            "id" INT NOT NULL,
            "name" VARCHAR(24) NOT NULL,
            "abbreviation" VARCHAR(5) NOT NULL,
            CONSTRAINT "unit_PK" PRIMARY KEY("id"),
            CONSTRAINT "unit_name_UQ" UNIQUE("name")
        )');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE "unit_id_seq" CASCADE');
        $this->addSql('DROP TABLE "unit"');
    }
}
