<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190403021231 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Create tables for Egress and EgressDetail';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE egress_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE egress (
	        id INT NOT NULL,
	        total NUMERIC(10, 2) NOT NULL,
	        egressed_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
	        CONSTRAINT egress_PK PRIMARY KEY(id)
        )');
        $this->addSql('CREATE SEQUENCE egress_detail_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE egress_detail (
            id INT NOT NULL,
            product_fk INT NOT NULL,
            egress_fk INT NOT NULL,
            quantity INT NOT NULL DEFAULT 1,
            unit_price NUMERIC(10, 2) NOT NULL DEFAULT 0.0,
            CONSTRAINT egress_detail_PK PRIMARY KEY(id),
            CONSTRAINT egress_detail_product_FK FOREIGN KEY (product_fk) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE,
            CONSTRAINT egress_detail_egress_FK FOREIGN KEY (egress_fk) REFERENCES egress (id) NOT DEFERRABLE INITIALLY IMMEDIATE
        )');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE egress_detail_id_seq CASCADE');
        $this->addSql('DROP TABLE egress_detail');
        $this->addSql('DROP SEQUENCE egress_id_seq CASCADE');
        $this->addSql('DROP TABLE egress');
    }
}
