<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190402212346 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Create tables for Request and RequestDetail';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE request_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE request_detail_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE request (
            id INT NOT NULL,
            provider_fk INT NOT NULL,
            code VARCHAR(100) NOT NULL,
            requested_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
            CONSTRAINT request_PK PRIMARY KEY(id),
            CONSTRAINT request_provider_FK FOREIGN KEY (provider_fk) REFERENCES provider (id) NOT DEFERRABLE INITIALLY IMMEDIATE
        )');
        $this->addSql('CREATE TABLE request_detail (
            id INT NOT NULL,
            product_fk INT NOT NULL,
            request_fk INT NOT NULL,
            quantity INT NOT NULL DEFAULT 1,
            CONSTRAINT request_detail_PK PRIMARY KEY(id),
            CONSTRAINT request_detail_product_FK FOREIGN KEY (product_fk) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE,
            CONSTRAINT request_detail_request_FK FOREIGN KEY (request_fk) REFERENCES request (id) NOT DEFERRABLE INITIALLY IMMEDIATE
        )');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE request_detail_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE request_id_seq CASCADE');
        $this->addSql('DROP TABLE request_detail');
        $this->addSql('DROP TABLE request');
    }
}
