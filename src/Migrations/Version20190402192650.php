<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190402192650 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Create table for Product';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE "product_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE "product" (
            "id" INT NOT NULL,
            "name" VARCHAR(150) NOT NULL,
            "model" VARCHAR(25) DEFAULT NULL,
            "serial" VARCHAR(100) DEFAULT NULL,
            "brand_fk" INT NOT NULL,
            "category_fk" INT NOT NULL,
            "unit_fk" INT NOT NULL,
            CONSTRAINT "product_PK" PRIMARY KEY("id"),
            CONSTRAINT "product_name_UQ" UNIQUE("name"),
            CONSTRAINT "product_brand_FK" FOREIGN KEY("brand_fk") REFERENCES "brand" ("id")  NOT DEFERRABLE INITIALLY IMMEDIATE,
            CONSTRAINT "product_category_FK" FOREIGN KEY("category_fk") REFERENCES "category" ("id")  NOT DEFERRABLE INITIALLY IMMEDIATE,
            CONSTRAINT "product_unit_FK" FOREIGN KEY("unit_fk") REFERENCES "unit" ("id")  NOT DEFERRABLE INITIALLY IMMEDIATE
        )');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE "product_id_seq" CASCADE');
        $this->addSql('DROP TABLE "product"');
    }
}
