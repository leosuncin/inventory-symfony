<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190402232645 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE entry_detail_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE entry_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE entry (
            id INT NOT NULL,
            comment TEXT DEFAULT NULL,
            added_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
            total NUMERIC(10, 2) NOT NULL,
            request_fk INT NOT NULL,
            CONSTRAINT entry_PK PRIMARY KEY(id),
            CONSTRAINT entry_request_FK FOREIGN KEY (request_fk) REFERENCES request (id) NOT DEFERRABLE INITIALLY IMMEDIATE
        )');
        $this->addSql('CREATE TABLE entry_detail (
            id INT NOT NULL,
            entry_fk INT NOT NULL,
            product_fk INT NOT NULL,
            quantity INT NOT NULL,
            unit_price NUMERIC(10, 2) NOT NULL,
            CONSTRAINT entry_detail_PK PRIMARY KEY(id),
            CONSTRAINT entry_detail_product_FK FOREIGN KEY (product_fk) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE,
            CONSTRAINT entry_detail_entry_FK FOREIGN KEY (entry_fk) REFERENCES entry (id) NOT DEFERRABLE INITIALLY IMMEDIATE
        )');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE entry_detail_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE entry_id_seq CASCADE');
        $this->addSql('DROP TABLE entry_detail');
        $this->addSql('DROP TABLE entry');
    }
}
