<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190408073503 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE sonata_user_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE sonata_group_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE sonata_user (
            id INT NOT NULL PRIMARY KEY,
            username VARCHAR(180) NOT NULL,
            username_canonical VARCHAR(180) NOT NULL UNIQUE,
            email VARCHAR(180) NOT NULL,
            email_canonical VARCHAR(180) NOT NULL UNIQUE,
            enabled BOOLEAN NOT NULL,
            salt VARCHAR(255) DEFAULT NULL,
            password VARCHAR(255) NOT NULL,
            last_login TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
            confirmation_token VARCHAR(180) DEFAULT NULL UNIQUE,
            password_requested_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
            roles TEXT NOT NULL,
            created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
            updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
            date_of_birth TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
            firstname VARCHAR(64) DEFAULT NULL,
            lastname VARCHAR(64) DEFAULT NULL,
            website VARCHAR(64) DEFAULT NULL,
            biography VARCHAR(1000) DEFAULT NULL,
            gender VARCHAR(1) DEFAULT NULL,
            locale VARCHAR(8) DEFAULT NULL,
            timezone VARCHAR(64) DEFAULT NULL,
            phone VARCHAR(64) DEFAULT NULL,
            facebook_uid VARCHAR(255) DEFAULT NULL,
            facebook_name VARCHAR(255) DEFAULT NULL,
            facebook_data JSON DEFAULT NULL,
            twitter_uid VARCHAR(255) DEFAULT NULL,
            twitter_name VARCHAR(255) DEFAULT NULL,
            twitter_data JSON DEFAULT NULL,
            gplus_uid VARCHAR(255) DEFAULT NULL,
            gplus_name VARCHAR(255) DEFAULT NULL,
            gplus_data JSON DEFAULT NULL,
            token VARCHAR(255) DEFAULT NULL,
            two_step_code VARCHAR(255) DEFAULT NULL
        )');
        $this->addSql('COMMENT ON COLUMN sonata_user.roles IS \'(DC2Type:array)\'');
        $this->addSql('CREATE TABLE sonata_group (
            id INT NOT NULL PRIMARY KEY,
            name VARCHAR(180) NOT NULL UNIQUE,
            roles TEXT NOT NULL
        )');
        $this->addSql('COMMENT ON COLUMN sonata_group.roles IS \'(DC2Type:array)\'');
        $this->addSql('CREATE TABLE fos_user_user_group (
            user_id INT NOT NULL,
            group_id INT NOT NULL,
            PRIMARY KEY(user_id, group_id),
            FOREIGN KEY (user_id) REFERENCES sonata_user (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE,
            FOREIGN KEY (group_id) REFERENCES sonata_group (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE
        )');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE sonata_user_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE sonata_group_id_seq CASCADE');
        $this->addSql('DROP TABLE fos_user_user_group');
        $this->addSql('DROP TABLE sonata_group');
        $this->addSql('DROP TABLE sonata_user');
    }
}
