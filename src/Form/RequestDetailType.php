<?php

namespace App\Form;

use App\Entity\RequestDetail;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RequestDetailType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('quantity', null, [
                'attr' => [
                    'min' => 1,
                    'step' => '1',
                ],
                'label' => 'Quantity',
            ])
            ->add('product', EntityType::class, [
                'class' => 'App\Entity\Product',
                'choice_label' => 'name',
                'label' => 'Product Name'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => RequestDetail::class,
        ]);
    }
}
