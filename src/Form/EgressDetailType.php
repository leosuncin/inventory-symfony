<?php

namespace App\Form;

use App\Entity\EgressDetail;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EgressDetailType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('product', EntityType::class, [
                'class' => 'App\Entity\Product',
                'choice_label' => 'name',
                'label' => 'Product Name'
            ])
            ->add('unitPrice', MoneyType::class, [
                'currency' => 'USD',
                'attr' => [
                    'min' => 0,
                    'step' => '0.01',
                ],
                'label' => 'Unit Price',
            ])
            ->add('quantity', null, [
                'attr' => [
                    'min' => 1,
                    'step' => '1',
                ],
                'label' => 'Quantity',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => EgressDetail::class,
        ]);
    }
}
