<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EgressRepository")
 */
class Egress
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(
     *     targetEntity="App\Entity\EgressDetail",
     *     mappedBy="egress",
     *     orphanRemoval=true,
     *     cascade={"persist", "remove"}
     * )
     */
    private $details;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $total = 0.00;

    /**
     * @ORM\Column(type="datetime")
     */
    private $egressedAt;

    public function __construct()
    {
        $this->egressedAt = new \DateTime();
        $this->details = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|EgressDetail[]
     */
    public function getDetails(): Collection
    {
        return $this->details;
    }

    public function addDetail(EgressDetail $detail): self
    {
        if (!$this->details->contains($detail)) {
            $this->details[] = $detail;
            $detail->setEgress($this);
        }

        return $this;
    }

    public function removeDetail(EgressDetail $detail): self
    {
        if ($this->details->contains($detail)) {
            $this->details->removeElement($detail);
            // set the owning side to null (unless already changed)
            if ($detail->getEgress() === $this) {
                $detail->setEgress(null);
            }
        }

        return $this;
    }

    public function getTotal()
    {
        return $this->total;
    }

    public function setTotal($total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getEgressedAt(): ?\DateTimeInterface
    {
        return $this->egressedAt;
    }

    public function setEgressedAt(\DateTimeInterface $egressedAt): self
    {
        $this->egressedAt = $egressedAt;

        return $this;
    }
}
