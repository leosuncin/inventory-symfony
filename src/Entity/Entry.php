<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EntryRepository")
 */
class Entry
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    /**
     * @ORM\Column(type="datetime")
     */
    private $addedAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Request")
     * @ORM\JoinColumn(name="request_fk", nullable=false)
     */
    private $request;

    /**
     * @ORM\OneToMany(
     *     targetEntity="App\Entity\EntryDetail",
     *     mappedBy="entry",
     *     orphanRemoval=true,
     *     cascade={"persist", "remove"}
     * )
     */
    private $details;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $total;

    public function __construct()
    {
        $this->addedAt = new \DateTime();
        $this->details = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getAddedAt(): ?\DateTimeInterface
    {
        return $this->addedAt;
    }

    public function setAddedAt(\DateTimeInterface $addedAt): self
    {
        $this->addedAt = $addedAt;

        return $this;
    }

    public function getRequest(): ?Request
    {
        return $this->request;
    }

    public function setRequest(Request $request): self
    {
        $this->request = $request;

        return $this;
    }

    /**
     * @return Collection|EntryDetail[]
     */
    public function getDetails(): Collection
    {
        return $this->details;
    }

    public function addDetail(EntryDetail $detail): self
    {
        if (!$this->details->contains($detail)) {
            $this->details[] = $detail;
            $detail->setEntry($this);
        }

        return $this;
    }

    public function removeDetail(EntryDetail $detail): self
    {
        if ($this->details->contains($detail)) {
            $this->details->removeElement($detail);
            // set the owning side to null (unless already changed)
            if ($detail->getEntry() === $this) {
                $detail->setEntry(null);
            }
        }

        return $this;
    }

    public function getTotal()
    {
        return $this->total;
    }

    public function setTotal($total): self
    {
        $this->total = $total;

        return $this;
    }
}
