<?php

namespace App\Repository;

use App\Entity\Kardex;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Kardex|null find($id, $lockMode = null, $lockVersion = null)
 * @method Kardex|null findOneBy(array $criteria, array $orderBy = null)
 * @method Kardex[]    findAll()
 * @method Kardex[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class KardexRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Kardex::class);
    }

    // /**
    //  * @return Kardex[] Returns an array of Kardex objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('k')
            ->andWhere('k.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('k.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Kardex
    {
        return $this->createQueryBuilder('k')
            ->andWhere('k.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
