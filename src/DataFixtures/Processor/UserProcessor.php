<?php


namespace App\DataFixtures\Processor;


use App\Entity\User;
use Fidry\AliceDataFixtures\ProcessorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

final class UserProcessor implements ProcessorInterface
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * UserProcessor constructor.
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @inheritDoc
     */
    public function preProcess(string $id, $object): void
    {
        if (!($object instanceof User)) return;
        $object->setPassword($this->passwordEncoder->encodePassword($object, $object->getPassword()));
    }

    /**
     * @inheritDoc
     */
    public function postProcess(string $id, $object): void
    {
        // TODO: Implement postProcess() method.
    }

}
